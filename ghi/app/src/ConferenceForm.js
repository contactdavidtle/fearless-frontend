import { useEffect, useState } from "react";

function ConferenceForm (props) {
  const [locations, setLocations] = useState([])

  const [name, setName] = useState('')
  const [start, setStart] = useState('')
  const [end, setEnd] = useState('')
  const [description, setDescription] = useState('')
  const [maxPresentations, setMaxPresentations] = useState('')
  const [maxAttendees, setMaxAttendees] = useState('')
  const [location, setLocation] = useState('')

  const handleNameChange = event => {
    const value = event.target.value
    setName(value)
  }

  const handleStartChange = event => {
    const value = event.target.value
    setStart(value)
  }

  const handleEndChange = event => {
    const value = event.target.value
    setEnd(value)
  }

  const handleDescriptionChange = event => {
    const value = event.target.value
    setDescription(value)
  }

  const handleMaxPresentationsChange = event => {
    const value = event.target.value
    setMaxPresentations(value)
  }

  const handleMaxAttendeesChange = event => {
    const value = event.target.value
    setMaxAttendees(value)
  }

  const handleLocationChange = event => {
    const value = event.target.value
    setLocation(value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    // create an empty JSON object
    const data = {};
    data.name= name
    data.starts = start
    data.ends = end
    data.description = description
    data.max_presentations = maxPresentations
    data.max_attendees = maxAttendees
    data.location = location


    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
      setName('')
      setStart('')
      setEnd('')
      setDescription('')
      setMaxPresentations('')
      setMaxAttendees('')
      setLocation('')

    }
  }


  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setLocations(data.locations)

    }
  }
    useEffect(() => {
        fetchData();
      }, []);

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new conference</h1>
        <form onSubmit={handleSubmit} id="create-conference-form">
          <div className="form-floating mb-3">
            <input onChange={handleNameChange} placeholder="Name" name="name" required type="text" id="name" className="form-control" value={name} />
            <label htmlFor="name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleStartChange} placeholder="Start" name="starts" required type="date" id="starts" className="form-control" value={start} />
            <label htmlFor="starts">Start of conference</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleEndChange} placeholder="End" name="ends" required type="date" id="ends" className="form-control" value={end} />
            <label htmlFor="ends">End of conference</label>
          </div>
          <div className="mb-3">
            <label htmlFor="description" className="form-label">Conference Description</label>
            <textarea onChange={handleDescriptionChange} className="form-control" id="description" rows="3" value={description}></textarea>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleMaxPresentationsChange} placeholder="Maximum presentations" name="max_presentations" required type="number" id="max_presentations" className="form-control" value={maxPresentations} />
            <label htmlFor="max_presentations">Maximum presentations</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleMaxAttendeesChange} placeholder="Maximum attendees" name="max_attendees" required type="number" id="max_attendees" className="form-control" value={maxAttendees} />
            <label htmlFor="max_attendees">Maximum attendees</label>
        </div>
        <div className="mb-3">
          <select onChange={handleLocationChange} required id="location" name="location" className="form-select" value={location}>
            <option key="default" value="">Choose a location</option>
            {locations.map(location => {
              return (
                <option key={location.id} value={location.id}>
                  {location.name}
                </option>
              );
            })}
          </select>
        </div>


          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>

  )

}

export default ConferenceForm
