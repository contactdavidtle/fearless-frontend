window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/locations/"

  const locationResponse = await fetch(url)

   if (locationResponse.ok) {

    const locationData = await locationResponse.json()
    console.log(locationData)
    const selectTag = document.getElementById("location")

    for (let location of locationData.locations) {
      const option = document.createElement("option")
      option.value = location.id
      option.innerHTML = location.name
      selectTag.append(option)
    }
   }

  const formTag = document.getElementById('create-conference-form');
  formTag.addEventListener('submit', async event => {
  event.preventDefault();

  const formData = new FormData(formTag);
  const json = JSON.stringify(Object.fromEntries(formData));

  const locationUrl = 'http://localhost:8000/api/conferences/';
  const fetchConfig = {
    method: "post",
    body: json,
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const locationResponse = await fetch(locationUrl, fetchConfig);
  if (locationResponse.ok) {
    formTag.reset();
    const newLocation = await locationResponse.json();
    console.log(newLocation)
  }
  });

 })
