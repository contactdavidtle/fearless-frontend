function createCard(name, description, pictureUrl, dateStart, dateEnd, locationName) {
  return `
    <div class="col">
      <div class="card shadow p-3 mb-5 bg-white rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          <p>${dateStart} - ${dateEnd}</p>
        </div>
      </div>
    </div>
  `;
}


function createPlaceHolder() {
  return `
    <div class="card" aria-hidden="true" id="placeholder">
      <img src="https://via.placeholder.com/150?text=loading" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title placeholder-glow">
          <span class="placeholder col-6"></span>
        </h5>
        <p class="card-text placeholder-glow">
          <span class="placeholder col-7"></span>
          <span class="placeholder col-4"></span>
          <span class="placeholder col-4"></span>
          <span class="placeholder col-6"></span>
          <span class="placeholder col-8"></span>
        </p>
        <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
      </div>
    </div>
  `;
}

function createError() {
  return `<div class="alert alert-primary" role="alert">No conferences retrieved.</div>`
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      const alert = document.querySelector('#alert-placeholder');
      alert.innerHTML += createError();

    } else {
      const data = await response.json();

      for (let x = 0; x < data.conferences.length; x++ ) {
        const column = document.querySelector('#conference-cards');
        column.innerHTML += createPlaceHolder()
      }

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const column = document.querySelector('#conference-cards');

          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const dateStart = new Date(details.conference.starts)
          const dateEnd = new Date(details.conference.ends)
          const dateStartStr = dateStart.toLocaleDateString()
          const dateEndStr = dateEnd.toLocaleDateString()
          const locationName = details.conference.location.name
          const html = createCard(name, description, pictureUrl, dateStartStr, dateEndStr, locationName);

          const placeHolder = document.querySelector('#placeholder')
          placeHolder.remove()
          column.innerHTML += html



        }
      }

    }
  } catch (e) {
    console.error("error", e)
  }

});
