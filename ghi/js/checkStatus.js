const payloadCookie = await cookieStore.get("jwt_access_payload");
if (payloadCookie) {
    // The cookie value is a JSON-formatted string, so parse it
    // My cookieStore.get() grabs an already parsed string rather than a JSON-formmatted string
    // so it doesn't need to be parsed.
    console.log("payloadCookie", payloadCookie)
    const encodedPayload = payloadCookie.value;
    console.log("encodedPayload", encodedPayload)
    // Convert the encoded payload from base64 to normal string
    const decodedPayload = atob(encodedPayload)
    console.log("decodedPayload", decodedPayload)
    // The payload is a JSON-formatted string, so parse it
    const payload = JSON.parse(decodedPayload)
    console.log("payload", payload)

    // Print the payload

    // Check if "events.add_conference" is in the permissions.
    // If it is, remove 'd-none' from the link

    const perms = payload.user.perms
    if (perms.includes("events.add_conference")) {
        const locationNav = document.getElementById('new_conference')
        locationNav.classList.remove("d-none")
    }
    if (perms.includes("events.add_location")) {
        const locationNav = document.getElementById('new_location')
        locationNav.classList.remove("d-none")
    }
    if (perms.includes("presentations.add_presentation")) {
        const locationNav = document.getElementById('new_presentation')
        locationNav.classList.remove("d-none")
    }

}
